#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 12 20:51:45 2017

@author: guiubu
"""

import genetico.sudoku as sd
import genetico.population as pop
import genetico.cruce as cru
import genetico.mutation as mut
import genetico.selection as sel
import genetico.tournament as tour


#improtamos el sudoku a resolver
#archivo=input("¿Cual es el archivo del sudoku?")
#creamos la instancia del sudoku inicial
sudoku= sd.Sudoku()
sudoku.set_FileContent('sudoku.txt')
#comprobamos que el contenido corresponde a lo importado
content= sudoku.get_Content()
#creamos una población de individuos indicados en el primer parámetro
population=pop.Population(100,sudoku)


#lista de valores para el plot
listavalores=[]


def evolucion(populationmeu,iteraciones):
    print('Ejecutando el algoritmo. Espere...')
    for i in range(0,iteraciones+1):
        if populationmeu.Best().getFitness()>0:
          
            listavalores.append(populationmeu.Best().getFitness())
            #se procede al torneo primer parametro k
            padres=tour.selectionTournament(3,populationmeu)
            #realizamos el cruce primer parametro probabilidad de cruce
            generacionMedia=cru.cruce(0.4,padres,sudoku)
            #realizamos la mutación probabilidad de mutacion
            generacionMutada=mut.mutation(0.02,generacionMedia,sudoku)
            #realizamos la selección
            generacionfinal=sel.selection(generacionMutada,populationmeu)
            #sustituimos la generación apra la siguiente iteración
            populationmeu=generacionfinal
         
        else:
            #si se encuentre el fitness 0 devolvemos el sudoku que será la solución
            print('found')
            print(iteraciones)
            listavalores.append(populationmeu.Best().getFitness())
            populationmeu.Best().printSudoku()
            break
    print('La búsqueda ha terminado')
evolucion(population,10000)


