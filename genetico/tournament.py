#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 17 10:53:50 2017

@author: guimi
"""

import random as rd
#selecciona los apdres mediante la seleccion por torneo
def selectionTournament(k,population):
        padres=[]
        for j in range(0,population.size):
            listaSel=[]
            for i in range(0,k):
                listaSel.append(population.get_Population()[rd.randint(0,population.size-1)])
            padres.append(mejorLista(listaSel))
        return padres
    #devuelve el mejor individuo de la lista
def mejorLista(lista):
    mejor=lista[0]
    for i in lista:
        if i.getFitness()<mejor.getFitness():
            mejor=i
   
    return mejor
