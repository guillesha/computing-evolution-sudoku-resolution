#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 13 10:34:33 2017

@author: guimi
"""
import genetico.sudoku as sud
import random as rd
class Population():
    #crea una nueva población con un tamaño predefinido
    
    def __init__(self,number=0,sudoku=0):
        self.main= sudoku
        self.Sudoku=sud.Sudoku
        self.population= []
        self.size=number
        if sudoku!=0:
            for x in range(number):
                sudoku= sud.Sudoku()
                sudoku.set_Content(self.randomSudoku())
                self.population.append(sudoku)
            self.best=self.Best()
            self.media=self.calcular_media()
    #añade un sudoku a la población
    def addSudoku(self,sudoku):
        self.size=self.size+1
        self.population.append(sudoku)
    
    def setPopulation(self,population):
        self.population=self.population+population
    #devuelve una lista con los individuos de la población
    def get_Population(self):
        return self.population
    def get_Best(self):
        return self.best
    #crea un individuo aleatorio
    def randomSudoku(self):
        main=self.main.get_Content()
        rSudoku=main[:]
        for i,val in enumerate(rSudoku):
             permitidos=[1,2,3,4,5,6,7,8,9]
             if val==0:
                 [x,y,z]=whereIsIt(i)
                 for t in self.main.fila[x-1]:
                     if permitidos.count(t)>0:  
                         permitidos.remove(t)
                 for j in self.main.columna[y-1]:
                     if permitidos.count(j)>0:        
                         permitidos.remove(j)
                 for k in self.main.celda[z-1]:
                     if permitidos.count(k)>0: 
                         permitidos.remove(k) 
                 rand=rd.choice(permitidos)
                 rSudoku[i]=rand
                
        return rSudoku    
    #calcula el mejor individuo de la población
    def Best(self):
        best=self.population[0]
        for i in self.population:
            if i.getFitness()<best.getFitness():
                best=i
        return best
    #calcula el peor individuo de la población
    def worst(self):
        worst=self.population[0]
        for i in self.population:
            if i.getFitness()>worst.getFitness():
                worst=i
        return worst
    def get_media(self):
        return self.media
    #calcula la media de la población
    def calcular_media(self):
        media=0
        for i in self.population:
            media=media+i.getFitness()
        return media/self.size
#esta función devuelve la fila columna o celda de cierta posición i
def whereIsIt(i):
    x=0
    y=0
    z=0
    fila=[1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,8,9,9,9,9,9,9,9,9,9]
    columna=[1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9]
    celda=[1,1,1,2,2,2,3,3,3,1,1,1,2,2,2,3,3,3,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,4,4,4,5,5,5,6,6,6,4,4,4,5,5,5,6,6,6,7,7,7,8,8,8,9,9,9,7,7,7,8,8,8,9,9,9,7,7,7,8,8,8,9,9,9,]
    x=fila[i]
    y=columna[i]
    z=celda[i]
    return x,y,z
                 
    

        
        
            