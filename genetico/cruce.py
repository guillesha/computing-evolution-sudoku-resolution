#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 15 12:07:15 2017

@author: guimi
"""
import genetico.sudoku as sd
import genetico.population as pop
import random as rd

def cruce(p,padresu,principal):

    padres=padresu[:]
    generacionMedia= pop.Population()
    contenidoPrincipal=[]
    contenidoPrincipal=contenidoPrincipal+principal.get_Content()
    permitidos= list(range(len(padres)))
    length=len(principal.get_Content())
    #bucle que para realizar el cruce con un rango de padres dividio por 2
    for i in range(0,int(len(padres)/2)):
        k=rd.randint(0,length)
        
        rand1=rd.choice(permitidos)
        permitidos.remove(rand1)
        rand2=rd.choice(permitidos)
        permitidos.remove(rand2)
        padre1=padres[rand1].get_Content()
        padre2=padres[rand2].get_Content()
       #comprobamos que la probabilidad es menor que cierta p
        if rd.uniform(0,1)<p:

            hijo1= sd.Sudoku()
            hijo2= sd.Sudoku()
            lista1=[]
            lista2=[]
            #bucle para realizar los cruces
            for i in range(0,length):
                #creación de los dos hijos
                if contenidoPrincipal[i]!=0:
                    lista1.append(contenidoPrincipal[i])
                    lista2.append(contenidoPrincipal[i])
                elif i<k:
                    lista1.append(padre1[i])
                    lista2.append(padre2[i])
                else:
                    lista1.append(padre2[i])
                    lista2.append(padre1[i])
            hijo1.set_Content(lista1)
            hijo2.set_Content(lista2)
            generacionMedia.addSudoku(hijo1)
            generacionMedia.addSudoku(hijo2) 
        else:
            #si la probabilidad es menor no se cruzan y se ñaden los dos apdres a la generacion media
            generacionMedia.addSudoku(padres[rand1])
            generacionMedia.addSudoku(padres[rand2])
       
    return generacionMedia