#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 12 20:52:09 2017

@author: guiubu
"""

class Sudoku:
    #inicializa el sudoku
    def __init__(self):
        self.content=0
        self.fitness=0
        self.fila=[[],[],[],[],[],[],[],[],[]]
        self.columna=[[],[],[],[],[],[],[],[],[]]
        self.celda=[[],[],[],[],[],[],[],[],[]]
        #inicializa el contenido
    def set_Content(self,content):
        self.content=content
        self.fitness=self.fitness_g()
        #lee el contenido pricnipal
    def set_FileContent(self,file):  
          with open(file) as f: 
            content=f.read()

          self.content= [int(x.strip()) for x in content]
	
          self.setFijos()
       #devuelv el conenido o lista de enteros
    def get_Content(self):
       return self.content
   #devuelve el valor de la función fitness
    def getFitness(self):
       return self.fitness
    def fitness_g(self):
        return (self.fi()+self.gi()+self.ci())/2
    def fi(self):
        fi=0
       
        fila=[self.content[i:i+9] for i in range(0,len(self.content),9)]
        for i in fila:
            fi=fi+iguales(i)
        return fi
    def gi(self):
        c1=0
        fi=0
        for j in range(0,9):
            columna=self.content[c1::9]
            fi=fi+iguales(columna)   
            c1=c1+1
          
        return fi
    def printSudoku(self):
        l=0
        l1=9
        for i in range (0,9):
            print(self.content[l:l1]),
            l=l+9
            l1=l1+9
        
    def ci(self):
      c11=0
      c22=1
      c33=2
      ci=0
      for j in range(0,3):
           for i in range(0,3):
               celda=[]
               celda=celda+self.content[c11:c11+19:9]
               celda=celda+self.content[c22:c22+19:9]
               celda=celda+self.content[c33:c33+19:9]
               c11=c11+3
               c22=c22+3
               c33=c33+3
               ci=ci+iguales(celda) 
           c11=c11+18
           c22=c22+18
           c33=c33+18
      return ci  
    def setFijos(self):
        index=0
        for i in self.content:
            if i!=0:
                
                [x,y,z]=whereIsIt(index)
               
                self.fila[x-1].append(i)
                self.columna[y-1].append(i)
                self.celda[z-1].append(i)

            index=index+1

        
         
def iguales(fcc):
    resultado=0
    for k in range(1,10):
        if fcc.count(k)>1:
            
            count=(fcc.count(k)-1)*fcc.count(k)
            resultado=resultado+count             
    return resultado    
               
    #creates a population of the specified number size
def whereIsIt(i):
    x=0
    y=0
    z=0
    fila=[1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,8,9,9,9,9,9,9,9,9,9]
    columna=[1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9]
    celda=[1,1,1,2,2,2,3,3,3,1,1,1,2,2,2,3,3,3,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,4,4,4,5,5,5,6,6,6,4,4,4,5,5,5,6,6,6,7,7,7,8,8,8,9,9,9,7,7,7,8,8,8,9,9,9,7,7,7,8,8,8,9,9,9,]
    x=fila[i]
    y=columna[i]
    z=celda[i]
    return x,y,z
     
    
    
