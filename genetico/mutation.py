#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 16 09:32:07 2017

@author: guimi
"""
import genetico.sudoku as sd
import genetico.population as pop
import random as rd
def mutation(p,generacionMedia,sudoku):
    generacionMutada= pop.Population()
    contenidoPrincipal=[]
    contenidoPrincipal=contenidoPrincipal+sudoku.get_Content()[:]
    for i in generacionMedia.get_Population():
        mutado= sd.Sudoku()
        mutado.set_Content(i.get_Content()[:])
        index=0
        for j in i.get_Content():
            permitidos=[1,2,3,4,5,6,7,8,9]
            #comprobamos la probabilidad de mutación
            if rd.uniform(0,1)<p and contenidoPrincipal[index]==0:
                 #comprobamos que el valor aññadido no se corresponde con uno de los fijos
                 [x,y,z]=whereIsIt(index)
                 for t in sudoku.fila[x-1]:
                     if permitidos.count(t)>0:  
                         permitidos.remove(t)
                 for l in sudoku.columna[y-1]:
                     if permitidos.count(l)>0:        
                         permitidos.remove(l)
                 for k in sudoku.celda[z-1]:
                     if permitidos.count(k)>0: 
                         permitidos.remove(k) 
                 rand=rd.choice(permitidos)
                 #cambiamos a un valor aleatorio
                 mutado.get_Content()[index]=rand
            index=index+1
            
        generacionMutada.addSudoku(mutado)
    return generacionMutada
#esta función devuelve la fila columna o celda de cierta posición i
def whereIsIt(i):
    x=0
    y=0
    z=0
    fila=[1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,8,9,9,9,9,9,9,9,9,9]
    columna=[1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9,1,2,3,4,5,6,7,8,9]
    celda=[1,1,1,2,2,2,3,3,3,1,1,1,2,2,2,3,3,3,1,1,1,2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,4,4,4,5,5,5,6,6,6,4,4,4,5,5,5,6,6,6,7,7,7,8,8,8,9,9,9,7,7,7,8,8,8,9,9,9,7,7,7,8,8,8,9,9,9,]
    x=fila[i]
    y=columna[i]
    z=celda[i]
    return x,y,z    