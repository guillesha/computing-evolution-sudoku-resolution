#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 16 13:24:11 2017

@author: guimi
"""

import genetico.population as pop
#funcion que realiza la sustitucion generacional
def selection(generacionMutada,generacionAnterior):
    sigGeneracion= pop.Population()
    peor=generacionMutada.worst()
    for i in generacionMutada.get_Population():
        if i is not peor:
            sigGeneracion.addSudoku(i)    
    sigGeneracion.addSudoku(generacionAnterior.Best())
    return sigGeneracion